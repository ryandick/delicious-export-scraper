# Export

Export bookmarks from delicious by going to

`https://export.delicious.com/settings/bookmarks/export`

# Scrape

Run the scraper with the file we downloaded when we exported our bookmarks

`node scrape.js delicious.html`

For this to work you must be running a phearjs server on the localhost on port 8100

# Serve

To expose the bookmark store over a http api run

`node webserver.js`

This includes a full text search powered by a lucern style index to create that index run

`node build_index.js`

# Save Store

To save the leveldb store to a json file run

`node export.js`
