'use strict';
const fs = require( 'fs' ),
	path = require( 'path' ),
	levelup = require( 'levelup' ),
	lunrIdx = require( './lib/lunr-index' ),
	fuzzysearch = require( 'fuzzysearch' ),
	Pace = require( 'pace' ),
	natural = require( 'natural' ),
	streamingWrite = require( 'json-streamify' ).streamingWrite,
	_ = require( 'lodash' );
const db = levelup( './bookmarks' );
const Trie = natural.Trie;
//  index configuration
const INDEX_LOCATION = path.resolve( './bookmark_index.json' );
let idx = lunrIdx( INDEX_LOCATION );
module.exports = {
	suggestionStore: {},
	suggestCount: 0,
	trie: new Trie(),
	spellChecker: null,
	set: function ( name, data, callback ) {
		db.put( name, data, {
			valueEncoding: 'json'
		}, callback );
	},
	get: function ( name, callback ) {
		db.get( name, callback );
	},
	generateTokens: function () {
		let blacklist = [ ';', '/', '\\', '\'', '!', '%', '`', '^', '*', '’', '\\n', '\\t', '\\', '\\\\', '\\\\n' ];
		let self = this;
		Object.keys( idx.documentStore.store ).forEach( key => {
			let tokens = idx.documentStore.store[ key ];
			if ( tokens.length > 0 ) {
				tokens.elements.forEach( t => {
					if ( t.length > 3 ) {
						t = String( t );
						let isBlacklisted = false;
						blacklist.forEach( b => {
							if ( t.indexOf( b ) > -1 ) isBlacklisted = true;
						} );
						if ( !isBlacklisted ) {
							self.suggestCount++;
							if ( self.suggestionStore[ t ] ) self.suggestionStore[ t ]++;
							else {
								self.suggestionStore[ t ] = 1;
								if ( !self.trie.contains( t ) ) self.trie.addString( t );
							}
						}
					}
				} );
			}
		} );
	},
	index: function ( callback ) {
		console.time( 'building search index' );
		idx = lunrIdx( INDEX_LOCATION, true );
		this.all( function ( err, items ) {
			var indexPace = new Pace( Object.keys( items ).length );
			db.createReadStream().on( 'data', function ( data ) {
				data = JSON.parse( data.value );
				if ( data.statusCode === 200 ) {
					idx.add( data );
				}
				indexPace.op();
			} ).on( 'error', function ( err ) {
				return callback( err );
			} ).on( 'close', function () {
				console.timeEnd( 'building search index' );
				console.log( 'writing index to: "' + INDEX_LOCATION + '"' );
				streamingWrite( INDEX_LOCATION, idx, function ( data ) {
					console.log( data );
					callback();
				} );
			} );
		} );
	},
	search: function ( query, callback ) {
		return ( callback ) ? callback( null, idx.search( query ) ) : idx.search( query );
	},
	suggest: function ( query, limit, callback ) {
		if ( this.suggestCount <= 0 ) this.generateTokens();
		let suggestCache = {};
		let suggestions = Object.keys( this.suggestionStore ).filter( t => t.length > query.length ).filter( t => fuzzysearch( query, t ) ).map( t => {
			t = t.trim().toLowerCase();
			suggestCache[ t ] = true;
			return {
				token: t,
				count: this.suggestionStore[ t ],
				idf: idx.idf( t ),
				distance: natural.JaroWinklerDistance( t, query )
			}
		} );
		// try and use trie to find more suggestions based on prefixes
		this.trie.findMatchesOnPath( query ).forEach( t => {
			t = t.trim().toLowerCase();
			if ( !suggestCache[ t ] ) suggestions.push( {
				token: t,
				count: this.suggestionStore[ t ],
				idf: idx.idf( t ),
				distance: natural.JaroWinklerDistance( t, query ),
			} );
		} );
		suggestions = _.sortBy( suggestions, suggestion => {
			return !!( ( suggestion.distance * suggestion.idf ) + suggestion.count );
		} );
		return callback( null, suggestions.max( limit ) );
	},
	all: function ( callback ) {
		let returnData = {};
		db.createReadStream().on( 'data', function ( data ) {
			returnData[ data.key ] = JSON.parse( data.value );
		} ).on( 'error', function ( err ) {
			return callback( err );
		} ).on( 'close', function () {
			return callback( null, returnData );
		} );
	}
}
