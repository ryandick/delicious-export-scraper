'use strict';
//	dependencies
const Store = require( './Store' ),
	express = require( 'express' ),
	bodyParser = require( 'body-parser' ),
	moment = require( 'moment' ),
	_ = require( 'lodash' ),
	truncate = require( './lib/truncate' ),
	Bookmarks = require( './services/Bookmarks' ),
	Logger = require( './services/Logger' );
require( './lib/extend-array' )( Array );
//	instances
const app = express(),
	logger = new Logger(),
	bookmarkService = new Bookmarks();
app.use( bodyParser() );
app.use( logger() );
/**
 * @route /api/1/bookmarks
 * @method GET
 * @querystring offset [Number]
 * @querystring limit [Number]
 */
app.get( '/api/1/bookmarks', ( req, res ) => {
	let bookmarks = bookmarkService.bookmarks;
	let bookmarkCount = bookmarkService.count;
	let start = +new Date();
	let data = Object.keys( bookmarks ).map( ( key, index ) => {
		let b = bookmarks[ key ];
		return {
			id: index,
			type: 'bookmark',
			attributes: {
				title: b.title,
				url: b.url,
				statusCode: b.statusCode,
				summary: truncate( b.summary, 250, true ),
				added: new Date( Number( b.added.toString() ) * 1000 ),
				updated: new Date( Number( b.updated.toString() ) )
			}
		}
	} ).filter( ( b ) => {
		return ( b.attributes.statusCode === 200 );
	} ).offset( Number( req.query.offset || 0 ) ).max( Number( req.query.limit || 25 ) );
	let currentPage = Number( req.query.offset || 0 ) / Number( req.query.limit || 25 )
	let totalPages = Math.ceil( bookmarkCount / ( Number( req.query.limit ) || 25 ) );
	return res.status( 200 ).send( {
		data: data,
		meta: {
			page: currentPage,
			nextPage: ( ( currentPage + 1 ) <= totalPages ) ? currentPage + 1 : null,
			prevPage: ( ( currentPage - 1 ) > 0 ) ? currentPage - 1 : 0,
			limit: Number( req.query.limit ) || 25,
			offset: Number( req.query.offset ) || 0,
			total: bookmarkCount,
			totalPages: totalPages,
			took: +new Date() - start
		}
	} );
} );
/**
 * @route /api/1/bookmarks/:bookmarkID
 * @method GET
 * @param bookmarkID [Number]
 */
app.get( '/api/1/bookmarks/:bookmarkID', ( req, res ) => {
	let bookmarks = bookmarkService.bookmarks;
	var bookmark = Object.keys( bookmarks ).map( ( key ) => {
		return bookmarks[ key ];
	} )[ Number( req.params.bookmarkID ) ];
	var data = {
		id: Number( req.params.bookmarkID ),
		type: 'bookmark',
		attributes: {
			title: bookmark.title,
			url: bookmark.url,
			statusCode: bookmark.statusCode,
			summary: truncate( bookmark.summary, 250, true ),
			text: bookmark.summary,
			added: new Date( Number( bookmark.added.toString() ) * 1000 ),
			updated: new Date( Number( bookmark.updated.toString() ) )
		}
	};
	return res.status( 200 ).send( {
		data: data
	} );
} );
/**
 * @route /api/1/searches
 * @method GET
 * @querystring query [String]
 */
app.get( '/api/1/searches', ( req, res ) => {
	let start = +new Date();
	let query = req.query.query;
	let bookmarks = bookmarkService.bookmarks;
	if ( query && query.length > 0 ) {
		let searchResult = Store.search( query );
		let data = searchResult.map( ( sr ) => {
			var tmpbookmark = bookmarks[ sr.ref ];
			tmpbookmark.id = Object.keys( bookmarks ).indexOf( sr.ref );
			tmpbookmark.score = sr.score;
			return tmpbookmark;
		} ).filter( ( b ) => {
			return ( b.statusCode === 200 );
		} ).map( ( b ) => {
			return {
				id: Number( b.id ),
				type: 'search',
				attributes: {
					query: query,
					url: b.url,
					title: b.title,
					statusCode: b.statusCode,
					summary: truncate( b.summary, 250, true ),
					added: new Date( Number( b.added.toString() ) * 1000 ),
					updated: new Date( Number( b.updated.toString() ) ),
					score: ( b.score * 100 )
				}
			};
		} ).sort( ( a, b ) => moment( a.attributes.added ).isBefore( b.attributes.added, 'day' ) );
		let resultCount = searchResult.length;
		let currentPage = Number( req.query.offset || 0 ) / Number( req.query.limit || 25 )
		let totalPages = Math.ceil( resultCount / ( Number( req.query.limit ) || 25 ) );
		let heatmap = data.map( b => moment( b.attributes.added ) ).map( b => moment( b ).format( 'DD-MM-YY' ) );
		let hist = {};
		heatmap.forEach( function ( a ) {
			if ( a in hist ) hist[ a ]++;
			else hist[ a ] = 1;
		} );
		let heatmapComplete = Object.keys( hist ).map( function ( key ) {
			return {
				date: moment( key, 'DD-MM-YY' ).startOf( 'day' ).toDate(),
				count: hist[ key ]
			};
		} );
		heatmapComplete = _.sortBy( heatmapComplete, 'date' );
		data = data.offset( Number( req.query.offset || 0 ) ).max( Number( req.query.limit || 25 ) );
		res.status( 200 ).send( {
			data: data,
			meta: {
				page: currentPage,
				totalPages: totalPages,
				nextPage: ( ( currentPage + 1 ) <= totalPages ) ? currentPage + 1 : null,
				prevPage: ( ( currentPage - 1 ) > 0 ) ? currentPage - 1 : 0,
				limit: Number( req.query.limit ) || 10,
				offset: Number( req.query.offset ) || 0,
				heatmap: heatmapComplete.offset( heatmapComplete.length - 60 ).max( 60 ),
				total: data.length,
				resultCount: resultCount,
				took: +new Date() - start,
				query: query
			}
		} );
	} else {
		res.status( 200 ).send( {
			data: [],
			meta: {
				total: 0,
				query: null
			}
		} );
	}
} );
/**
 * @route /api/1/suggests
 * @method GET
 * @querystring query [String]
 * @querystring limit [Number]
 */
app.get( '/api/1/suggests', ( req, res ) => {
	let start = +new Date();
	let query = req.query.query;
	let limit = ( req.query.limit > 10 ) ? 10 : req.query.limit;
	Store.suggest( query, limit, ( err, suggestions ) => {
		res.status( 200 ).send( {
			data: suggestions.map( ( s, si ) => {
				return {
					id: si,
					type: 'suggest',
					attributes: s
				}
			} ),
			meta: {
				total: suggestions.length,
				query: query,
				took: +new Date() - start
			}
		} );
	} );
} );
let server = app.listen( 4100, () => {
	var host = server.address().address;
	var port = server.address().port;
	console.log( `webserver listening on ${host}:${port}` );
} );
