module.exports = function ( Array ) {
	Array.prototype.max = function ( mx ) {
		return this.filter( ( e, i ) => {
			return i < mx;
		} );
	};
	Array.prototype.offset = function ( os ) {
		return this.filter( ( e, i ) => {
			return i > os - 1
		} );
	};
	return Array;
};
