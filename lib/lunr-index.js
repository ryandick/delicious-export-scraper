'use strict';
const lunr = require( 'lunr' ),
	fs = require( 'fs' );
module.exports = function ( indexPath, dontLoadIndex ) {
	let idx = lunr( function () {
		this.ref( 'url' );
		this.field( 'title', {
			boost: 15
		} );
		this.field( 'text', {
			boost: 5
		} );
		this.field( 'tags', {
			boost: 15
		} );
		this.field( 'url', {
			boost: 5
		} );
		this.field( 'summary', {
			boost: 10
		} );
	} );
	//	load tokens and docs from stored index on disk
	if ( idx.corpusTokens.elements.length === 0 && !dontLoadIndex ) {
		idx = lunr.Index.load( JSON.parse( fs.readFileSync( indexPath ) ) );
	}
	return idx;
}
