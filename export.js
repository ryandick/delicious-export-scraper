var Store = require( './Store' ),
  fs = require( 'fs' ),
  path = require( 'path' );
Store.all( function ( err, items ) {
  if ( err ) throw err;
  fs.writeFile( path.resolve( 'export-output.json' ), JSON.stringify( items ), function ( err ) {
    if ( err ) throw err;
    console.log( 'exported results to: "' + path.resolve( 'export-output.json' ) + '"' );
  } );
} );
