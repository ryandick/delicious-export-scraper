'use strict';
const fs = require( 'fs' ),
	path = require( 'path' ),
	cheerio = require( 'cheerio' ),
	url = require( 'url' ),
	request = require( 'request' ),
	unfluff = require( 'unfluff' ),
	glossary = require( "glossary" )( {
		minFreq: 2,
		collapse: true
	} ),
	summary = require( 'node-summary' ),
	async = require( 'neo-async' ),
	argv = process.argv[ 2 ],
	nocache = ( process.argv[ 3 ] ) ? process.argv[ 3 ] : false,
	Pace = require( 'pace' ),
	Store = require( './Store' ),
	PHEAR_SERVER = process.env.PHEAR_SERVER || 'localhost',
	PHEAR_PORT = process.env.PHEAR_PORT || 8100;
if ( argv ) {
	var filepath = path.resolve( argv );
	if ( fs.existsSync( filepath ) ) {
		console.log( 'using phear server: "http://' + PHEAR_SERVER + ':' + PHEAR_PORT + '"' )
		var fileData = fs.readFileSync( filepath ).toString( 'utf8' );
		var results = parseExportData( fileData );
		Store.all( function ( err, storeData ) {
			if ( err ) throw err;
			var phearjsTargets = phearjsTargetsFromResults( results, storeData, nocache );
			console.log( 'read ' + results.length + ' bookmarks from: "' + filepath + '"' );
			console.log( Object.keys( storeData ).length + ' bookmarks already scraped' );
			submitTargetsToPhear( phearjsTargets, function ( err, phearResponses ) {
				if ( err ) throw err;
				else fs.writeFile( path.resolve( 'output.json' ), JSON.stringify( phearResponses ) );
			} );
		} );
	}
} else console.log( 'please supply a file' );

function parseExportData( fileData ) {
	var $ = cheerio.load( fileData );
	var results = [];
	$( "A" ).each( function ( i, image ) {
		results.push( {
			href: $( image ).attr( 'href' ),
			tags: $( image ).attr( 'tags' ).split( ',' ),
			added: $( image ).attr( 'add_date' ),
			updated: new Date().getTime()
		} );
	} );
	return results;
}

function phearjsTargetsFromResults( results, storeData, cache ) {
	return results.filter( function ( u ) {
		return ( storeData[ u.href ] && !cache ) ? false : true;
	} ).map( function ( u ) {
		u.crawlURL = 'http://' + PHEAR_SERVER + ':' + PHEAR_PORT + '?fetch_url=' + encodeURIComponent( u.href );
		return u;
	} );
}

function submitTargetsToPhear( targets, callback ) {
	let count = 0;
	let pace = new Pace( targets.length );
	async.mapLimit( targets, 5, function ( target, next ) {
		count++;
		var url = target.crawlURL;
		var cleanedUrl = decodeURIComponent( url.replace( 'http://' + PHEAR_SERVER + ':' + PHEAR_PORT + '?fetch_url=', '' ) );
		request.get( url, function ( err, res, body ) {
			if ( err ) return next( err );
			var resObj = unfluff( body );
			resObj.url = cleanedUrl;
			resObj.statusCode = res.statusCode;
			resObj.tags = target.tags;
			resObj.added = target.added;
			resObj.updated = target.updated;
			summary.summarize( resObj.title, resObj.text, function ( err, summaryText ) {
				if ( err ) {
					pace.op();
					return next( err );
				} else {
					resObj.summary = summaryText;
					if ( summaryText ) {
						try {
							glossary.extract( summaryText ).forEach( function ( tag ) {
								resObj.tags.push( tag );
							} );
						} catch ( e ) {
							console.error( e );
						}
					}
					Store.set( cleanedUrl, resObj, function ( err ) {
						pace.op();
						return next( err, resObj );
					} );
				}
			} );
		} );
	}, callback );
}
