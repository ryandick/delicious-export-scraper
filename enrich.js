'use strict';
const Store = require( './Store' ),
	fs = require( 'fs' ),
	path = require( 'path' ),
	salient = require( 'salient' ),
	glossary = new salient.glossary.Glossary(),
	Pace = require( 'pace' ),
	nasync = require( 'neo-async' );
Store.all( ( err, items ) => {
	if ( err ) throw err;
	let itemsArray = Object.keys( items ).map( key => items[ key ] );
	let pace = new Pace( itemsArray.length );
	console.log( `enriching ${itemsArray.length} bookmarks tags` );
	nasync.mapLimit( itemsArray, 1, ( item, next ) => {
		if ( item.text.length > 39 ) {
			glossary.parse( item.text );
			let itemTags = glossary.concepts();
			itemTags.forEach( tag => {
				if ( item.tags.indexOf( tag ) === -1 ) item.tags.push( tag );
			} );
			Store.set( item.url, item, function ( item ) {
				pace.op();
				next( null, item );
			} );
		} else next( null, item );
	}, ( err, results ) => {
		console.log( 'done' );
		process.exit();
	} );
} );
