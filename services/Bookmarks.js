'use strict';
const Store = require( '../Store' );

function Bookmarks() {
	Store.all( ( err, returnedBookmarks ) => {
		if ( err ) throw err;
		this.bookmarks = returnedBookmarks;
		this.count = Object.keys( returnedBookmarks ).length;
		console.log( `${this.count} bookmarks found` );
	} );
	return this;
}
module.exports = Bookmarks;
