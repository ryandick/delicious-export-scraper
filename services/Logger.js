'use strict';

function Logger() {
	return function () {
		return function ( req, res, next ) {
			let start = +new Date();
			res.on( 'finish', () => {
				let took = +new Date() - start;
				console.log( `[${req.method}]-> ${req.url} ${took}ms` );
			} )
			next();
		}
	}
}
module.exports = Logger;
